package com.account.transcation.model;

import java.math.BigDecimal;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.account.transcation.constants.CommonConstants;
import com.account.transcation.validation.ValidAccountNumber;


public class AccountRequest {

	//@NotBlank
	//@Size(min = 10,max = 12, message =CommonConstants.ACCOUNT_NUM_VALIDATION_ERR )
	@ValidAccountNumber
	private String accountNumber;
	
	@NotNull
	@Min(message =CommonConstants.AMOUNT_VALIDATION_ERR, value = 1 )
	@Digits(integer = 8, fraction = 2,message = CommonConstants.AMOUNT_VALIDATION_DECIMAL_ERR)
	private BigDecimal balance;

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	
	
	
}
