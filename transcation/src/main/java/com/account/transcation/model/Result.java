package com.account.transcation.model;

import java.util.ArrayList;
import java.util.List;

import com.account.transcation.dao.entity.Account;
import com.account.transcation.dao.entity.Transaction;

public class Result {
	
	private Transaction transaction;
	private String transactionStatus;
	private Account account;
	private List<Message> messages;


	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public List<Message> getMessages() {
		if(messages==null) {
			messages = new ArrayList<Message>();
		}
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}
	
	
	
	
	

}
