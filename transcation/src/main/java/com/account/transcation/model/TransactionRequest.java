package com.account.transcation.model;

import java.math.BigDecimal;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.account.transcation.constants.CommonConstants;
import com.account.transcation.validation.ValidAccountNumber;

public class TransactionRequest {
	
	//@NotBlank
	//@Size(min = 10,max = 12, message ="Debit Account number is manadatory & must be alteast 10 digits or max 12 digits" )
	@ValidAccountNumber
	private String debitAccountId;
	
	//@NotBlank
	//@Size(min = 10,max = 12, message ="Credit Account number is manadatory & must be alteast 10 digits or max 12 digits" )
	@ValidAccountNumber
	private String creditAccountId;
	
	@NotNull
	@Min(value = 1, message = CommonConstants.AMOUNT_VALIDATION_ERR)
	@Digits(integer = 8, fraction = 2,message = CommonConstants.AMOUNT_VALIDATION_DECIMAL_ERR)
	private BigDecimal amount;
	
	@NotBlank
	@Size(min = 2,max = 30, message =CommonConstants.DECSRIPTION_VALIDATION_ERR )
	private String description;

	public String getDebitAccountId() {
		return debitAccountId;
	}

	public void setDebitAccountId(String debitAccountId) {
		this.debitAccountId = debitAccountId;
	}

	public String getCreditAccountId() {
		return creditAccountId;
	}

	public void setCreditAccountId(String creditAccountId) {
		this.creditAccountId = creditAccountId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	

}
