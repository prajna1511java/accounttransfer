package com.account.transcation.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.account.transcation.dao.entity.Account;

@Repository
public interface AccountTransferRepository extends JpaRepository<Account, String>{
	
	public Account findByAccountNumber(String accountNumber);
	
	 

}
