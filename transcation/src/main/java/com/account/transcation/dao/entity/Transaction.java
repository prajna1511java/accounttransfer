package com.account.transcation.dao.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TRANSACTION_HISTORY")
public class Transaction {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long transactionId;
	
	@Column(name = "TRANSACTION_AMOUNT")
	private BigDecimal amount;
	
	@Column(name = "DEBIT_ACCOUNT_NUMBER")
	private String debitAccountNumber;
	
	@Column(name = "CREDIT_ACCOUNT_NUMBER")
	private String creditAccountNumber;
	
	@Column(name = "DEBIT_ACCOUNT_BALANCE")
	private BigDecimal debitAccountBalance;
	
	@Column(name = "CREDIT_ACCOUNT_BALANCE")
	private BigDecimal creditAccountBalance;
	
	@Column(name = "TRANSACTION_DATE")
	private LocalDate transactionDate;

	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getDebitAccountNumber() {
		return debitAccountNumber;
	}

	public void setDebitAccountNumber(String debitAccountNumber) {
		this.debitAccountNumber = debitAccountNumber;
	}

	public String getCreditAccountNumber() {
		return creditAccountNumber;
	}

	public void setCreditAccountNumber(String creditAccountNumber) {
		this.creditAccountNumber = creditAccountNumber;
	}

	public BigDecimal getDebitAccountBalance() {
		return debitAccountBalance;
	}

	public void setDebitAccountBalance(BigDecimal debitAccountBalance) {
		this.debitAccountBalance = debitAccountBalance;
	}

	public BigDecimal getCreditAccountBalance() {
		return creditAccountBalance;
	}

	public void setCreditAccountBalance(BigDecimal creditAccountBalance) {
		this.creditAccountBalance = creditAccountBalance;
	}

	public LocalDate getTransactionDate() {
		if(transactionDate==null) {
			transactionDate = LocalDate.now();
		}
		return transactionDate;
	}

	public void setTransactionDate(LocalDate transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Transaction(Long transactionId, BigDecimal amount, String debitAccountNumber, String creditAccountNumber,
			BigDecimal debitAccountBalance, BigDecimal creditAccountBalance, LocalDate transactionDate) {
		super();
		this.transactionId = transactionId;
		this.amount = amount;
		this.debitAccountNumber = debitAccountNumber;
		this.creditAccountNumber = creditAccountNumber;
		this.debitAccountBalance = debitAccountBalance;
		this.creditAccountBalance = creditAccountBalance;
		this.transactionDate = transactionDate;
	}

	public Transaction() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	
	
	
	
}
