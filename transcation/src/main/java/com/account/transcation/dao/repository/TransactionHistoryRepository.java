package com.account.transcation.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.account.transcation.dao.entity.Transaction;

public interface TransactionHistoryRepository extends JpaRepository<Transaction, Long> {
	

}
