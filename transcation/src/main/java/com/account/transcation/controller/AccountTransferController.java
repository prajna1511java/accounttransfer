package com.account.transcation.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.account.transcation.constants.CommonConstants;
import com.account.transcation.dao.entity.Account;
import com.account.transcation.model.AccountRequest;
import com.account.transcation.model.Result;
import com.account.transcation.model.TransactionRequest;
import com.account.transcation.service.AccountTransferService;

@RestController
public class AccountTransferController {

	private static final Logger logger = LoggerFactory.getLogger(AccountTransferController.class);
	
	@Autowired
	private AccountTransferService accountTransferService;
	
	
	
	@RequestMapping(value = "/account/transfer", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Result> accountTransfer(@RequestBody @Valid TransactionRequest transactionRequest, BindingResult bindingResult) {
		Result response = new Result();
		try {
			logger.info(" Inside accountTransfer method :");
			if(bindingResult.hasErrors()) {
				return accountTransferService.validationError(bindingResult, response);
			}
			
			return accountTransferService.accountTransfer(transactionRequest, response);
		}catch (Exception e) {
			accountTransferService.buildResult(response, CommonConstants.EXCEPTION, CommonConstants.EXCEPTION, CommonConstants.INTERNAL_SERVER_ERR);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			
		}
		
	}
	
	@RequestMapping(value = "/account/addbalance", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Result> addBalance(@RequestBody @Valid AccountRequest accountRequest, BindingResult bindingResult) {
		Result response = new Result();
		try {
			logger.info(" Inside addAccount method :");
			if(bindingResult.hasErrors()) {
				return accountTransferService.validationError(bindingResult, response);
			}
			response.setAccount(accountTransferService.addBalance(accountRequest));
			response.setTransactionStatus(CommonConstants.SUCCESS);
			return ResponseEntity.status(HttpStatus.OK).body(response);
		}catch (Exception e) {
			accountTransferService.buildResult(response, CommonConstants.EXCEPTION, CommonConstants.EXCEPTION, CommonConstants.INTERNAL_SERVER_ERR);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			
		}
		
	}
	
	
	@RequestMapping(value = "/account/checkbalance", method = RequestMethod.GET)
	public ResponseEntity<Result> checkBalance(@RequestParam(value = "accountNumber") String accountNumber) {
		Result response = new Result();
		try {
			logger.info(" Inside addAccount method :");
			Account account = accountTransferService.checkBalance(accountNumber);
			if(account!=null) {
				response.setAccount(account);
				response.setTransactionStatus(CommonConstants.SUCCESS);
				return ResponseEntity.status(HttpStatus.OK).body(response);
			}else {
				response.setTransactionStatus(CommonConstants.ERROR);
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
			}
			
		}catch (Exception e) {
			accountTransferService.buildResult(response, CommonConstants.EXCEPTION, CommonConstants.EXCEPTION, CommonConstants.INTERNAL_SERVER_ERR);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			
		}
		
	}
	
	
	
	
	
}
