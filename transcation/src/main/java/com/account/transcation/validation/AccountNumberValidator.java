package com.account.transcation.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class AccountNumberValidator implements ConstraintValidator<ValidAccountNumber, String>{

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		// TODO Auto-generated method stub
		
		 if(StringUtils.isNotBlank(value) && value.matches("[0-9\\-]+")  && value.length()>=10 && value.length()<=12) {
			return true;
		}
		return false;
	}
	
	
}
