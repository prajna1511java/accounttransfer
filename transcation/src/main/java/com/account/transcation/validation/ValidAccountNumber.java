package com.account.transcation.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.account.transcation.constants.CommonConstants;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Constraint(validatedBy = AccountNumberValidator.class)
public @interface ValidAccountNumber {

	String message() default CommonConstants.ACCOUNT_NUM_VALIDATION_ERR;
	
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}
