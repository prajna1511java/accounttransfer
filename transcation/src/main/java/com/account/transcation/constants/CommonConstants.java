package com.account.transcation.constants;

import org.springframework.stereotype.Component;

@Component
public final class CommonConstants {

	public static final String ERROR = "Error";
	public static final String SUCCESS = "Success";
	public static final String EXCEPTION = "Exception";
	public static final String ACCOUNT_NOT_FOUND_ERR ="Account not found";
	public static final String SAME_ACCOUNT_ERR ="Same account transfer is invalid";
	public static final String BALANCE_LOW_ERR="Account Balance is low to transfer amount";
	public static final String DECSRIPTION_VALIDATION_ERR="Description cannot be blank & must be alteast 2 characters";
	public static final String AMOUNT_VALIDATION_ERR="Minimum amount should be 1";
	public static final String ACCOUNT_NUM_VALIDATION_ERR="Account Number is invalid must be min 10 digits & max 12 digits";
	public static final String ACCOUNT_ADDED = "Account Added succesfully";
	public static final String INTERNAL_SERVER_ERR = "500- internal error occurred";
	public static final String AMOUNT_VALIDATION_DECIMAL_ERR ="Amount should be upto two decimals";
	
}
