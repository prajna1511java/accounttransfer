package com.account.transcation.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import com.account.transcation.constants.CommonConstants;
import com.account.transcation.dao.entity.Account;
import com.account.transcation.dao.entity.Transaction;
import com.account.transcation.dao.repository.AccountTransferRepository;
import com.account.transcation.dao.repository.TransactionHistoryRepository;
import com.account.transcation.model.AccountRequest;
import com.account.transcation.model.Message;
import com.account.transcation.model.Result;
import com.account.transcation.model.TransactionRequest;

@Service
public class AccountTransferService {
	
	private static final Logger logger = LoggerFactory.getLogger(AccountTransferService.class);
	
	@Autowired
	private AccountTransferRepository accountTransferRepository;
	
	@Autowired
	private TransactionHistoryRepository transactionHistoryRepository;
	

	
	
	public ResponseEntity<Result> accountTransfer(TransactionRequest transactionRequest, Result transactionResult) throws Exception{
		logger.info(" Inside accountTransfer method :");
		Account debitAccount = accountTransferRepository.findByAccountNumber(transactionRequest.getDebitAccountId());
		
		Account creditAccount  = accountTransferRepository.findByAccountNumber(transactionRequest.getCreditAccountId());
		if(debitAccount==null || creditAccount == null) {
			buildResult(transactionResult, CommonConstants.ERROR, CommonConstants.ERROR, CommonConstants.ACCOUNT_NOT_FOUND_ERR);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(transactionResult);
		}
		
		if(debitAccount.getAccountNumber().equals(creditAccount.getAccountNumber())) {
			buildResult(transactionResult, CommonConstants.ERROR, CommonConstants.ERROR, CommonConstants.SAME_ACCOUNT_ERR);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(transactionResult);
		}

		if(debitAccount.getBalance().compareTo(transactionRequest.getAmount())<0) {
			buildResult(transactionResult, CommonConstants.ERROR, CommonConstants.ERROR,CommonConstants.BALANCE_LOW_ERR);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(transactionResult);
		}

		debitAccount.setBalance(debitAccount.getBalance().subtract(transactionRequest.getAmount()));
		accountTransferRepository.save(debitAccount);
		creditAccount.setBalance(creditAccount.getBalance().add(transactionRequest.getAmount()));
		accountTransferRepository.save(creditAccount);
		transactionResult.setTransaction(postTransaction(transactionRequest, debitAccount, creditAccount));
		
		buildResult(transactionResult, CommonConstants.SUCCESS, CommonConstants.SUCCESS, 
				"Amount transferred succesfully from "+ transactionRequest.getDebitAccountId()+" to "+ transactionRequest.getCreditAccountId());
		return ResponseEntity.status(HttpStatus.OK).body(transactionResult);
	}

	private Transaction postTransaction(TransactionRequest transactionRequest, Account debitAccount, Account creditAccount) {
		Transaction transaction = new Transaction();
		transaction.setAmount(transactionRequest.getAmount());
		transaction.setDebitAccountNumber(transactionRequest.getDebitAccountId());
		transaction.setCreditAccountNumber(transactionRequest.getCreditAccountId());
		transaction.setDebitAccountBalance(debitAccount.getBalance());
		transaction.setCreditAccountBalance(creditAccount.getBalance());
		return transactionHistoryRepository.save(transaction);
	}
	
	public void buildResult(Result result, String messageType, String code, String message) {
		result.setTransactionStatus(messageType);
		Message msg = new Message(messageType,code,message);
		result.getMessages().add(msg);
	}
	
	public Account addBalance(AccountRequest accountRequest) throws Exception{
		Account account = accountTransferRepository.findByAccountNumber(accountRequest.getAccountNumber());
		if(account!=null) {
			account.setBalance(accountRequest.getBalance());
		}else {
			account = new Account();
			account.setAccountNumber(accountRequest.getAccountNumber());
			account.setBalance(accountRequest.getBalance());
		}
		
		
		return accountTransferRepository.save(account);
	}
	
	public Account checkBalance(String accountNumber) throws Exception{
		 if(StringUtils.isNotBlank(accountNumber) && accountNumber.matches("[0-9\\-]+")  && accountNumber.length()>=10 && accountNumber.length()<=12) {
			 return accountTransferRepository.findByAccountNumber(accountNumber);
			}
		return null;
	}
	
	public ResponseEntity<Result> validationError(BindingResult bindingResult,
			Result result) {
		logger.info(" Inside validationError method :");
		result.setTransactionStatus(CommonConstants.ERROR);
		List<ObjectError> errors = bindingResult.getAllErrors();
		for(ObjectError error : errors) {
			Message msg = new Message("ERROR", error.getCode(), error.getDefaultMessage());
			result.getMessages().add(msg);
		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(result);
	}
	
	

}
